Description:

Python program that uses recursion to find all solutions to the n-Queens problem, for 1 ≤ 𝑛 ≤ 13. The program will operate in two modes: normal and verbose (which is indicated by the command line option "-v"). In normal mode, the program prints only the number of solutions to n-Queens. In verbose mode, all solutions to n-Queens will be printed in the order they are found by the algorithm, and in a format described below, followed by the number of such solutions. Thus to find the number of solutions to 8-Queens you will type: $ python3 Queens.py 8 To print all 92 unique solutions to 8-Queens type: $ python3 Queens.py –v 8. 


Files:

README.txt
description of 8_Queens.py

8_Queens.py
assembly code 


Instructions: 

This program should be run on a python IDE and use the terminal.

