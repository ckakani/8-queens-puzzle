#------------------------------------------------------------------------------ 
#  8-Queens.py
#  solves the n queens problem either outputs the number of solutions
#  or the solutions
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# import statements
#------------------------------------------------------------------------------
import sys
#------------------------------------------------------------------------------
# definitions of optional helper functions
#------------------------------------------------------------------------------
def solutions(B):
    S = []
    for k in range(0,len(B)):
        for l in range(0, len(B)):
            if B[k][l] == 1:
                S.append(l)
    return S
def usage():
   print("Usage: python3 8_Queens.py [-v] number", file=sys.stderr)
   exit()
#------------------------------------------------------------------------------
# definitions of required functions, classes etc..
#------------------------------------------------------------------------------
def placeQueen(B,i,j):
    for l in range(i+1, len(B)):
        B[l][j] = -1
    for l in range(j,len(B)):
        B[l-1][l] -=1
    for l, k in zip(range(i, len(B)-1, 1), range(j, -1, -1)):
        B[l][k] -=1
    B[i][j] = 1

def removeQueen(B,i,j):
    for l in range(1, len(B)):
        B[l][j]  +=1
    for l in range(j,len(B)):
        B[l-1][l] +=1
    for l, k in zip(range(i, len(B)-1, 1), range(j, -1, -1)):
        B[l][k] +=1  
    B[i][j] = 0
def printBoard(n):
    B = []
    R = [] 
    for i in range(0, n+1):
        R.append(0)
    for j in range(0,n+1):
        B.append(R[:])
    return B

def findSolutions(B,i,mode):
    accumululated_sum = 0
    if i>n:
        if mode == "verbose":
            printBoard()
        return 1
    else:
        for B[i][j] in B[i]:
            if B[i][j] == 0:
                placeQueen(B)
                solutions(B,i+1,j)
                accumuluating_sum+=1
                removeQueen(B,i,j)
    print(str(len(B)) + " queens has " + str(accumulated_sum) + "solutions")

#------------------------------------------------------------------------------
# definition of function main()
#------------------------------------------------------------------------------
def main():
    mode = None
   # do whatever it is the program is supposed to do
    if len(sys.argv)==1 and type(sys.argv[1]) == int:
        mode = "verbose"
        out = sys.stdout
    elif sys.argv[1]=="-v" and len(sys.argv)==2:
        usage()
    # end
    if mode == "verbose":
        findSolutions(printBoard(out),out,out)


# end

#------------------------------------------------------------------------------
# closing conditional that calls main()
#------------------------------------------------------------------------------
if __name__=='__main__':

   main()

# end
